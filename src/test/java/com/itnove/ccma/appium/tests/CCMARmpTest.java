package com.itnove.ccma.appium.tests;

import com.itnove.ccma.appium.base.MobileBrowserBaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.AssertJUnit.assertEquals;

/**
 * CCMADirecteTest for Advanced Video on CCMA
 *
 * @author guillem.hernandez
 */
public class CCMARmpTest extends MobileBrowserBaseTest {

    @Test
    public void testRmpCarregaModul() {

        driver.get("http://www.ccma.cat/qa/modul/rmp");

        // Comprovem que tot i tenir N moduls carregats del mateix modul en la pàgina nomes fa un únic include del javascript
        List<WebElement> js = driver.findElements(By.xpath("//script[starts-with(@src,'//statics.ccma.cat/js/modul-rmp.min.js')]"));
        assertEquals("El total d'scripts corresponent al mòdul no es únic", 1, js.size());

        //Esperem que reaccioni el player que té l'autoplay
        new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#rmp2.tvcplayer.playstate")));

        // Comprovem que carrega un modul revisant el id corresponent
        WebElement firstRMP = driver.findElement(By.xpath("//div[@id='rmp1']"));

        String ValorModulClass = firstRMP.getAttribute("class");
        assertEquals("El class del modul no conté l'esperat: tvcplayer", true, ValorModulClass.contains("tvcplayer"));

        // Comprovem que la pàgina te el modul carregat N vegades
        List<WebElement> tvcplayer = driver.findElements(By.xpath("//div[contains(@class,'videoviewbase')]"));
        assertEquals("El total de moduls carregats no es l'esperat", 10, tvcplayer.size());

        List<WebElement> video = driver.findElements(By.cssSelector(".tvcplayer.videoplayer"));
        assertEquals("El total de moduls de video no és l'esperat", 6, video.size());

        List<WebElement> audio = driver.findElements(By.cssSelector(".tvcplayer.audioplayer"));
        assertEquals("El total de moduls d'audio no és l'esperat", 4, audio.size());

        List<WebElement> playing = driver.findElements(By.cssSelector(".tvcplayer.playstate"));
        assertEquals("El total de moduls reproduint no és l'esperat", 1, playing.size());
    }

}
