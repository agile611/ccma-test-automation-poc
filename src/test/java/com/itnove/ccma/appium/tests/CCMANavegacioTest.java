package com.itnove.ccma.appium.tests;

import com.itnove.ccma.appium.base.MobileBrowserBaseTest;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

/**
 * CCMA Accept GDPR Cookies
 *
 * @author guillem.hernandez
 */
public class CCMANavegacioTest extends MobileBrowserBaseTest {

    @Test
    public void testAcceptaCookies() throws InterruptedException {
        Thread.sleep(1000);
        driver.findElement(By.xpath("//a[@class='mesNoticies loadHbs']")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("acceptaPrefs")).click();
        Thread.sleep(1000);
        driver.findElement(By.xpath("//a[@id='tancaMissatgeCookie']")).click();
        Thread.sleep(1000);
    }
}
