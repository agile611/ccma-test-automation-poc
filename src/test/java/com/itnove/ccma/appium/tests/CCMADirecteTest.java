package com.itnove.ccma.appium.tests;

import com.itnove.ccma.appium.base.JWPlayer;
import com.itnove.ccma.appium.base.MobileBrowserBaseTest;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import static org.junit.Assert.assertTrue;

/**
 * CCMADirecteTest for Advanced Video on CCMA
 *
 * @author guillem.hernandez
 */
public class CCMADirecteTest extends MobileBrowserBaseTest {

    @Test
    public void testVideoAvanzado() throws Exception {
        driver.get("http://www.ccma.cat/tv3/directe/324/");
        Thread.sleep(25000);
        WebElement play = driver.findElement(By.cssSelector("#view25 > div.jw-controls.jw-reset > div.jw-display.jw-reset > div > div > div.jw-display-icon-container.jw-display-icon-display.jw-background-color.jw-reset > div"));
        play.click();
        play.click();
//        Actions action = new Actions(driver);
//        action.moveToElement(play).moveToElement(play).doubleClick().build().perform();
//        Thread.sleep(25000);

//
//        JWPlayer jw = new JWPlayer(driver);
//        jw.play();
//        jw.play();
//        assertTrue(jw.assertStatePlay());
//        assertTrue(jw.assertStatePlaying());
//        assertTrue(jw.assertStatePause());
//        assertTrue(jw.assertStatePlay());
//        assertTrue(jw.assertStateMuteTrue());
//        assertTrue(jw.assertStateMuteFalse());
//        assertTrue(jw.assertVolume(50));
//        assertTrue(jw.assertVolume(0));
//        assertTrue(jw.assertVolume(100));
    }
}
