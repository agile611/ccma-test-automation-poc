package com.itnove.ccma.appium.base;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileBrowserType;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

/**
 * MobileBrowserBaseTest Class for device setup and selection
 *
 * @author guillem.hernandez
 */
public class MobileBrowserBaseTest {
    public AppiumDriver driver;
    public static LocalRemoteWebDriverWait wait;
    public static JavascriptExecutor jse;
    public static long timeOut = 30;

    @BeforeMethod
    public void setUp() throws MalformedURLException {
        String device = System.getProperty("device");
	    String platformVersion = System.getProperty("version");
        // switch between different browsers, e.g. iOS Safari or Android Chrome
        // let's use the os name to differentiate, because we only use default browser in that os
        if (device != null && device.equalsIgnoreCase("Android")) {
            String deviceName = System.getProperty("deviceName");
            DesiredCapabilities capabilities = new DesiredCapabilities();
            capabilities.setCapability("deviceName", deviceName);
            capabilities.setCapability("platformVersion", platformVersion);
            capabilities.setCapability("udid", deviceName);  
            capabilities.setCapability("noReset", "true");
            capabilities.setCapability("skipUnlock", "true");
            capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, MobileBrowserType.CHROME);
            driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), capabilities);
        }
        else if (device.equalsIgnoreCase("ipadPro")) {
            DesiredCapabilities caps = DesiredCapabilities.ipad();
            caps.setCapability(MobileCapabilityType.APPIUM_VERSION, "1.8.0");
            caps.setCapability(MobileCapabilityType.BROWSER_NAME, "Safari");
            caps.setCapability(MobileCapabilityType.PLATFORM_NAME, "iOS");
            caps.setCapability(MobileCapabilityType.AUTOMATION_NAME, "XCUITest");
            caps.setCapability(MobileCapabilityType.DEVICE_NAME, "iPad Pro");
            caps.setCapability(MobileCapabilityType.VERSION, "11.3");
            caps.setCapability("xcodeOrgId", "D58QAT62JH");//Corporació Catalana de Mitjans Audiovisuals, SA
            caps.setCapability("xcodeSigningId", "iPhone Developer");
            caps.setCapability("startIWDP", true);
            caps.setCapability("udid", "8b97fbabac79444d0df665d6f4d14146928e8e2a");
            caps.setCapability("autoWebView", true);
            driver = new IOSDriver(new URL("http://0.0.0.0:4723/wd/hub"), caps);
        }
        else if (device.equalsIgnoreCase("ipad3")) {
            DesiredCapabilities caps = DesiredCapabilities.ipad();
            caps.setCapability(MobileCapabilityType.APPIUM_VERSION, "1.8.0");
            caps.setCapability(MobileCapabilityType.BROWSER_NAME, "Safari");
            caps.setCapability(MobileCapabilityType.PLATFORM_NAME, "iOS");
            caps.setCapability(MobileCapabilityType.AUTOMATION_NAME, "XCUITest");
            caps.setCapability(MobileCapabilityType.DEVICE_NAME, "iPad3");
            caps.setCapability(MobileCapabilityType.VERSION, "9.3.5");
            caps.setCapability("xcodeOrgId", "D58QAT62JH");//Corporació Catalana de Mitjans Audiovisuals, SA
            caps.setCapability("xcodeSigningId", "iPhone Developer");
            caps.setCapability("startIWDP", true);
            caps.setCapability("udid", "24a8b22b5596d95e2b90eaf9955c527efeea96ff");
            caps.setCapability("autoWebView", true);
            driver = new IOSDriver(new URL("http://0.0.0.0:4723/wd/hub"), caps);
        }
        else if (device.equalsIgnoreCase("iphone4s")) {
            DesiredCapabilities caps = DesiredCapabilities.ipad();
            caps.setCapability(MobileCapabilityType.APPIUM_VERSION, "1.8.0");
            caps.setCapability(MobileCapabilityType.BROWSER_NAME, "Safari");
            caps.setCapability(MobileCapabilityType.PLATFORM_NAME, "iOS");
            caps.setCapability(MobileCapabilityType.AUTOMATION_NAME, "XCUITest");
            caps.setCapability(MobileCapabilityType.DEVICE_NAME, "iPhone 4s");
            caps.setCapability(MobileCapabilityType.VERSION, "8.4.1");
            caps.setCapability("xcodeOrgId", "D58QAT62JH");//Corporació Catalana de Mitjans Audiovisuals, SA
            caps.setCapability("xcodeSigningId", "iPhone Developer");
            caps.setCapability("startIWDP", true);
            caps.setCapability("udid", "16ffb5fea0d4ee8132f392fb32a6e1c7df16cd05");
            caps.setCapability("autoWebView", true);
            driver = new IOSDriver(new URL("http://0.0.0.0:4723/wd/hub"), caps);
        }
        else {
            DesiredCapabilities caps = DesiredCapabilities.iphone();
            caps.setCapability(MobileCapabilityType.APPIUM_VERSION, "1.8.0");
            caps.setCapability(MobileCapabilityType.BROWSER_NAME, "Safari");
            caps.setCapability(MobileCapabilityType.PLATFORM_NAME, "iOS");
            caps.setCapability(MobileCapabilityType.AUTOMATION_NAME, "XCUITest");
            caps.setCapability(MobileCapabilityType.DEVICE_NAME, "iPhone 7");
            caps.setCapability(MobileCapabilityType.VERSION, "11.4");
            caps.setCapability("xcodeOrgId", "D58QAT62JH");//Corporació Catalana de Mitjans Audiovisuals, SA
            caps.setCapability("xcodeSigningId", "iPhone Developer");
            caps.setCapability("startIWDP", true);
            caps.setCapability("udid", "578cee58f9ca470480b1a73eb66165ba0a646453");
            caps.setCapability("autoWebView", true);
            driver = new IOSDriver(new URL("http://0.0.0.0:4723/wd/hub"), caps);
        }
        jse = driver;
        wait = new LocalRemoteWebDriverWait(driver, timeOut);
        driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(timeOut, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(timeOut, TimeUnit.SECONDS);
        driver.get("http://www.ccma.cat");
        try {
            Thread.sleep(25000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @AfterMethod
    public void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }
}