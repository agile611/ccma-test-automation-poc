package com.itnove.ccma.har.base;

import net.lightbody.bmp.BrowserMobProxy;
import net.lightbody.bmp.BrowserMobProxyServer;
import net.lightbody.bmp.client.ClientUtil;
import net.lightbody.bmp.core.har.Har;
import net.lightbody.bmp.core.har.HarEntry;
import net.lightbody.bmp.proxy.CaptureType;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.util.List;

public class FirefoxHarTest {

    @Test
    public void test() {

        System.setProperty("webdriver.gecko.driver",
                "src" + File.separator + "main"
                        + File.separator + "resources"
                        + File.separator + "geckodriver-macos");


        BrowserMobProxy proxy = getProxyServer(); //getting browsermob proxy
        System.out.println("BrowserMob Proxy running on port: " + proxy.getPort());

        Proxy seleniumProxy = getSeleniumProxy(proxy);

        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(CapabilityType.PROXY, seleniumProxy);
        capabilities.setCapability("acceptInsecureCerts", true);

        WebDriver driver = new FirefoxDriver(capabilities);

        proxy.newHar("FirefoxHarTest");
        driver.get("http://www.ccma.cat/324");


        Har har = proxy.getHar();

        // Write HAR Data in a File
        String harFilePath = "src" + File.separator + "main"
                + File.separator + "resources"
                + File.separator + "FirefoxHarTest.har";
        File harFile = new File(harFilePath);
        try {
            har.writeTo(harFile);
        } catch (IOException ex) {
            System.out.println(ex.toString());
            System.out.println("Could not find file " + harFilePath);
        }

        List<HarEntry> entries = har.getLog().getEntries();
        for (HarEntry entry : entries) {
            System.out.println("Request URL: " + entry.getRequest().getUrl());
            System.out.println("Entry response status: " + entry.getResponse().getStatus());
            System.out.println("Entry response text: " + entry.getResponse().getStatusText());

        }


        proxy.stop();
        driver.quit();
    }


    public Proxy getSeleniumProxy(BrowserMobProxy proxyServer) {
        Proxy seleniumProxy = ClientUtil.createSeleniumProxy(proxyServer);
        try {
            String hostIp = Inet4Address.getLocalHost().getHostAddress();
            seleniumProxy.setHttpProxy(hostIp + ":" + proxyServer.getPort());
            seleniumProxy.setSslProxy(hostIp + ":" + proxyServer.getPort());
        } catch (UnknownHostException e) {
            e.printStackTrace();
            Assert.fail("invalid Host Address");
        }
        return seleniumProxy;
    }

    public BrowserMobProxy getProxyServer() {
        BrowserMobProxy proxy = new BrowserMobProxyServer();
        proxy.setTrustAllServers(true);
        proxy.setHarCaptureTypes(CaptureType.REQUEST_CONTENT, CaptureType.RESPONSE_CONTENT);
        proxy.start();
        return proxy;
    }
}